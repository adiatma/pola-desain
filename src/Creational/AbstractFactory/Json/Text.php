<?php

namespace Adiatma\Pola\Creational\AbstractFactory\Json;

use Adiatma\Pola\Creational\AbstractFactory\Text as BaseText;

class Text extends BaseText
{
	/**
	* Untuk merender output text ke bentuk JSON
	* 
	* @return string
	*/
	public function render()
	{
		return json_encode(['content' => $this->text]);
	}
}