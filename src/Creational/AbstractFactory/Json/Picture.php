<?php

namespace Adiatma\Pola\Creational\AbstractFactory\Json;

use Adiatma\Pola\Creational\AbstractFactory\Picture as BasePicture;

class Picture extends BasePicture
{
	/**
	* Untuk merender output picture ke dalam bentuk JSON
	*
	* @return JSON
	*/
	public function render()
	{
		return json_encode(['title' => $this->name, 'path' => $this->path]);
	} 
}