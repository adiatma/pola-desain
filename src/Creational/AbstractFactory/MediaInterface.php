<?php

namespace Adiatma\Pola\Creational\AbstractFactory;

interface MediaInterface
{
	/**
	* Untuk merender output dari Json / Html. 
	*
	* @return string 
	*/
	public function render();
}