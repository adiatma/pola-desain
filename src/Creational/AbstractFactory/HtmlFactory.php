<?php

namespace Adiatma\Pola\Creational\AbstractFactory;

class HtmlFactory extends AbstractFactory
{
	/**
	* Membuat component content.
	*
	* @param string $content
	* @return Html\Text\Text
	*/
	public function createText($content)
	{
		return new Html\Text($content);
	}

	/**
	* Membuat component picture.
	*
	* @param string $path
	* @param string $name
	* @return Html\Picture\Picture
	*/
	public function createPicture($path, $name = '')
	{
		return new Html\Picture($path, $name);
	}
}