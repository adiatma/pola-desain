<?php

namespace Adiatma\Pola\Creational\AbstractFactory;

class JsonFactory extends AbstractFactory
{
	/**
	* Membuat component content.
	*
	* @param string $content
	* @return Json\Text\Text
	*/
	public function createText($content)
	{
		return new Json\Text($content);
	}

	/**
	* Membuat component picture.
	*
	* @param string $path
	* @param string $name 
	* @return Json\Picture\Picture
	*/
	public function createPicture($path, $name = '')
	{
		return new Json\Picture($path, $name);
	}
}