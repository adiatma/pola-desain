<?php

namespace Adiatma\Pola\Creational\AbstractFactory\Html;


use Adiatma\Pola\Creational\AbstractFactory\Text as BaseText;


class Text extends BaseText
{
	/**
	* Untuk merender output dari text kedalam bentuk html
	*
	* 
	*/
	public function render()
	{
		return '<div>' . htmlspecialchars($this->text) . '</div>';
	}
}