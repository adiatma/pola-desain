<?php

namespace Adiatma\Pola\Creational\AbstractFactory\Html;

use Adiatma\Pola\Creational\AbstractFactory\Picture as BasePicture;

class Picture extends BasePicture
{
	public function render()
	{
		/**
		* Untuk merender picture ke bentuk html.
		*
		* @return Html
		*/
		return sprintf('<img src="%s" title="%s"/>', $this->path, $this->name);
	}
}