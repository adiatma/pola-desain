<?php

namespace Adiatma\Pola\Creational\AbstractFactory;

abstract class AbstractFactory
{
	/**
	* Membuat component content
	*
	* @param string $content
	* @return string
	*/
	abstract public function createText($content);

	/**
	* Membuat component picture
	*
	* @param string $path
	* @param string $name
	* @return Picture
	*/
	abstract public function createPicture($path, $name = '');
}