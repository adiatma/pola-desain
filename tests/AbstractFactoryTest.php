<?php

namespace Adiatma\Tests;

use Adiatma\Pola\Creational\AbstractFactory\AbstractFactory;
use Adiatma\Pola\Creational\AbstractFactory\HtmlFactory;
use Adiatma\Pola\Creational\AbstractFactory\JsonFactory;

class AbstractFactoryTest extends \PHPUnit_Framework_TestCase
{
	public function getFactories()
	{
		return array(
			[new JsonFactory()],
			[new HtmlFactory()]
		);
	}

	/**
	* Sebagai client untuk mengetes componen creation
	*
	* @dataProvider getFactories
	*/
	public function testComponentCreation(AbstractFactory $factory)
	{
		$article = [
			$factory->createText('lorem ipsume'),
			$factory->createPicture('/image.jpg','caption'),
		];

		$this->assertContainsOnly('Adiatma\Pola\Creational\AbstractFactory\MediaInterface', $article);
	}
}